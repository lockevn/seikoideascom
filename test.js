// component: "article"
// component: "article_overview";
// component: "button"
// component: "call_to_action"
// component: "feature"
// component: "feature_list"
// component: "footer"
// component: "global"
// component: "header"
// component: "Hero"
// component: "nav_item"
// component: "page"

var a = {
  stories: [
    {
      name: "Article 1",
      created_at: "2018-12-27T14:48:03.838Z",
      published_at: "2019-01-15T11:46:00.698Z",
      alternates: [],
      id: 477354,
      uuid: "26a5991c-511d-41a1-b4e8-bd0199b6dd6f",
      content: {
        _uid: "04f1db0f-07ff-4ff4-a419-3d071a3d186d",
        intro:
          "Far far away, behind the word mountains, far from the countries [Vokalia](#) and [Consonantia](#), there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.",
        title: "This is the title",
        author: "Lockevn",
        component: "article",
        long_text:
          "n the example above, we're using the `user.id` from the server to generate the routes but tossing out the rest of the data. Typically, we need to fetch it again from inside the `/users/_id.vue`. While we can do that, we'll probably need to set the `generate.interval` to something like `100` in order not to flood the server with calls. Because this will increase the run time of the generate script, it would be preferable to pass along the entire `user` object to the context in `_id.vue`. We do that by modifying the code above to this:\n\nFar far away, behind the word mountains, far from the countries [Vokalia](#) and [Consonantia](#), there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.",
        categories: ["2a863bf7-b15c-4ec9-b273-aa9d0f646092"],
        teaser_image: "//a.storyblok.com/f/43698/2240x1354/92e71f7bfc/article.jpg"
      },
      slug: "article-1",
      full_slug: "article/article-1",
      sort_by_date: null,
      position: 0,
      tag_list: [],
      is_startpage: false,
      parent_id: 477348,
      meta_data: null,
      group_id: "e4f0d8d6-9cd2-4ad1-bf83-40d2864df16f",
      first_published_at: "2018-05-22T08:24:01.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Article 2",
      created_at: "2018-12-27T14:48:03.838Z",
      published_at: "2019-01-02T09:26:51.716Z",
      alternates: [],
      id: 477356,
      uuid: "c9c5a54d-d60d-49e0-b9f1-cb14cf802a4f",
      content: {
        _uid: "04f1db0f-07ff-4ff4-a419-3d071a3d186d",
        intro:
          "Far far away, behind the word mountains, far from the countries [Vokalia](#) and [Consonantia](#), there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.",
        title: "This is the second Title",
        author: {
          id: "3a526768-1d22-447e-9ddd-f4158a035edd",
          url: "",
          linktype: "story",
          fieldtype: "multilink",
          cached_url: "authors/john-doe"
        },
        component: "article",
        long_text:
          "Far far away, behind the word mountains, far from the countries [Vokalia](#) and [Consonantia](#), there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.",
        categories: ["0b87cb6f-b089-4dd2-a1e3-87f9427d0700"],
        teaser_image: "//a.storyblok.com/f/43698/2240x1354/92e71f7bfc/article.jpg"
      },
      slug: "article-2",
      full_slug: "article/article-2",
      sort_by_date: null,
      position: -30,
      tag_list: [],
      is_startpage: false,
      parent_id: 477348,
      meta_data: null,
      group_id: "617e8417-a10c-433e-a61e-67bdd55f7f1b",
      first_published_at: "2018-04-16T10:03:05.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Article 3",
      created_at: "2018-12-27T14:48:03.838Z",
      published_at: "2019-01-02T09:27:07.408Z",
      alternates: [],
      id: 477355,
      uuid: "304e15aa-59e0-4270-a9ec-bfb40951aa23",
      content: {
        _uid: "04f1db0f-07ff-4ff4-a419-3d071a3d186d",
        intro:
          "Far far away, behind the word mountains, far from the countries [Vokalia](#) and [Consonantia](#), there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.",
        title: "This is our third Title",
        author: {
          id: "3a526768-1d22-447e-9ddd-f4158a035edd",
          url: "",
          linktype: "story",
          fieldtype: "multilink",
          cached_url: "authors/john-doe"
        },
        component: "article",
        long_text:
          "Far far away, behind the word mountains, far from the countries [Vokalia](#) and [Consonantia](#), there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.",
        categories: ["0b87cb6f-b089-4dd2-a1e3-87f9427d0700"],
        teaser_image: "//a.storyblok.com/f/43698/2240x1354/92e71f7bfc/article.jpg"
      },
      slug: "article-3",
      full_slug: "article/article-3",
      sort_by_date: null,
      position: -20,
      tag_list: [],
      is_startpage: false,
      parent_id: 477348,
      meta_data: null,
      group_id: "61fd1cb2-f31f-4e07-9ac3-0ee88d86296a",
      first_published_at: "2018-04-16T10:03:16.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Articles Index",
      created_at: "2018-12-27T14:48:03.838Z",
      published_at: "2019-01-02T09:01:30.683Z",
      alternates: [],
      id: 477357,
      uuid: "d637be7f-8187-4e8b-9434-93390541f42b",
      content: {
        _uid: "8d7a1b28-87b8-4d60-8629-f04bbc288d14",
        headline: "Company news",
        component: "article_overview",
        background: "//a.storyblok.com/f/51818/960x313/5038f18e0a/news.jpg"
      },
      slug: "article",
      full_slug: "article/",
      sort_by_date: null,
      position: -40,
      tag_list: [],
      is_startpage: true,
      parent_id: 477348,
      meta_data: null,
      group_id: "a4f52a5c-1761-4130-847a-4353bc3273cd",
      first_published_at: "2018-04-13T17:38:33.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "__CONFIG__ GLOBAL Layout",
      created_at: "2018-12-27T14:48:03.800Z",
      published_at: "2019-01-04T03:01:06.596Z",
      alternates: [],
      id: 477351,
      uuid: "59666d20-8280-41d8-87c5-a1b6fd231d2b",
      content: {
        _uid: "7e6a5d2a-279a-48bb-a019-8c9f8689454d",
        footer: [
          {
            _uid: "812e200c-d686-47c3-9eaf-7fb1d6608a28",
            body: [
              {
                _uid: "e5fe4bd2-a465-4f0a-999d-6c28dcfe8329",
                icon: {
                  _uid: "85d1b5dd-66de-4610-a196-780779ba7664",
                  icon: "",
                  type: "fas",
                  plugin: "fontawesome-selector"
                },
                link: {
                  id: "7367d065-7499-411d-84c8-7141670e16c3",
                  url: "#",
                  _uid: "3b907066-e81e-4989-8348-41c8270af7c4",
                  linktype: "story",
                  fieldtype: "multilink",
                  cached_url: "home"
                },
                name: "Home",
                image: "",
                component: "nav_item"
              },
              {
                _uid: "f08b3b59-7aba-4432-b7a9-9c1047004bd1",
                icon: {
                  _uid: "3b1eccf2-32d1-45be-848b-6244ff85062d",
                  icon: "",
                  type: "fas",
                  plugin: "fontawesome-selector"
                },
                link: {
                  id: "803ee6ad-487e-4f1e-9a6e-eb9f20b6df34",
                  url: "#",
                  _uid: "1a7b0358-6bc5-42b1-b541-ebfe7f74f3ca",
                  linktype: "story",
                  fieldtype: "multilink",
                  cached_url: "corporate/legal"
                },
                name: "Legal",
                image: "",
                component: "nav_item"
              },
              {
                _uid: "a44bd657-5d5e-4e9f-b93c-5f832a25dbf5",
                icon: {
                  _uid: "16a575c7-0c8e-400f-bd30-95ee84d0b62c",
                  icon: "",
                  type: "fas",
                  plugin: "fontawesome-selector"
                },
                link: {
                  id: "46a69221-5b2a-45ac-80a6-b74f2d3bd3ff",
                  url: "#",
                  _uid: "04084c72-ab84-4b4a-b3e7-5195deda813a",
                  linktype: "story",
                  fieldtype: "multilink",
                  cached_url: "corporate/privacy-policy"
                },
                name: "Privacy Policy",
                image: "",
                component: "nav_item"
              },
              {
                _uid: "614ea1a2-6dbf-4c5a-9e0a-e93137bfe666",
                icon: {
                  _uid: "5f47a346-df6d-4e2c-ab03-0bb2b836a991",
                  icon: "",
                  type: "fas",
                  plugin: "fontawesome-selector"
                },
                link: {
                  id: "5e5d48b0-337b-4133-9387-5dbc65bbd658",
                  url: "sitemap/",
                  _uid: "cb99e830-52f7-46c9-b728-af2057fe24e2",
                  linktype: "url",
                  fieldtype: "multilink",
                  cached_url: "sitemap/"
                },
                name: "Sitemap",
                image: "",
                component: "nav_item"
              }
            ],
            style: ["footer-small", "bg-dark"],
            component: "footer"
          }
        ],
        header: [
          {
            _uid: "2651eb25-213b-4216-8d83-354812817602",
            logo: "//a.storyblok.com/f/51818/300x113/eb2a366be0/seiko-ideas_logo_s.png",
            style: [],
            styles: "",
            component: "header",
            hide_navi: false,
            logo_link: {
              id: "",
              url: "/",
              _uid: "5fa0110f-0dd3-47d9-9eb5-e35c792571af",
              linktype: "url",
              fieldtype: "multilink",
              cached_url: "/"
            },
            nav_links: [
              {
                _uid: "64ed580c-1b7a-4105-95e7-47f7a01400c9",
                icon: {
                  _uid: "28694fe8-3e3d-4fbd-9148-3101497072d3",
                  icon: "",
                  type: "fas",
                  plugin: "fontawesome-selector"
                },
                link: {
                  id: "7367d065-7499-411d-84c8-7141670e16c3",
                  url: "/",
                  _uid: "a323fdb7-a44c-4f74-99d0-dff1a53fe114",
                  linktype: "story",
                  fieldtype: "multilink",
                  cached_url: "home"
                },
                name: "Home",
                image: "",
                component: "nav_item"
              },
              {
                _uid: "84083da7-010a-4f59-b1fc-53a1b0f173fd",
                icon: {
                  _uid: "9205c5ba-9f48-45ed-9afe-1247cdc4fb5b",
                  icon: "",
                  type: "fas",
                  plugin: "fontawesome-selector"
                },
                link: {
                  id: "d637be7f-8187-4e8b-9434-93390541f42b",
                  url: "",
                  linktype: "story",
                  fieldtype: "multilink",
                  cached_url: "article/"
                },
                name: "News",
                image: "",
                component: "nav_item"
              }
            ],
            logo_style: ["navbar-brand"],
            button_link: {
              id: "",
              url: "https://www.google.com/forms/about/",
              _uid: "f559d218-2983-42a3-9ac8-1c2bc5144ac1",
              linktype: "url",
              fieldtype: "multilink",
              cached_url: "https://www.google.com/forms/about/"
            },
            button_text: "Contact us",
            navbar_style: ["navbar-expand-md"],
            main_nav_style: ["mr-auto"],
            container_style: [],
            second_nav_links: [
              {
                _uid: "c66c9c19-5399-42e8-bde0-4d2406b9aaf8",
                icon: {
                  _uid: "e13e37cb-95b8-4fc0-8700-0abc4a9edc1a",
                  icon: "fa-star",
                  type: "fas",
                  plugin: "fontawesome-selector"
                },
                link: {
                  id: "8e64a7bf-2a61-477d-ab67-bce4e46ad1a6",
                  url: "#",
                  _uid: "311a8f48-d197-4218-b95f-442f9b6b992f",
                  linktype: "story",
                  fieldtype: "multilink",
                  cached_url: "corporate/strength"
                },
                name: "Our advantages",
                image: "",
                component: "nav_item"
              },
              {
                _uid: "2c21524e-2636-4d6c-aede-ed251be63257",
                icon: {
                  _uid: "90ec3822-1ecc-42e7-a680-196a8da718ba",
                  icon: "fa-ambulance",
                  type: "fas",
                  plugin: "fontawesome-selector"
                },
                link: {
                  id: "9681538d-0da3-47ec-88b7-7e9e742e6183",
                  url: "#",
                  _uid: "d61e7e24-93c1-4ab0-9335-51a32f2e8ced",
                  linktype: "story",
                  fieldtype: "multilink",
                  cached_url: "service/"
                },
                name: "Services",
                image: "",
                component: "nav_item"
              },
              {
                _uid: "62336790-3986-4c35-ae84-43d82a475e00",
                icon: {
                  _uid: "27880538-a72b-4952-853f-6ba105f67ea9",
                  icon: "fa-address-card",
                  type: "fas",
                  plugin: "fontawesome-selector"
                },
                link: {
                  id: "493af7ba-9524-4a8c-80db-20b6426dab94",
                  url: "https://github.com/storyblok/bootstrap-demo",
                  _uid: "33825ffa-3268-4c12-9666-c4eda9d6da3d",
                  linktype: "story",
                  fieldtype: "multilink",
                  cached_url: "corporate/"
                },
                name: "About us",
                image: "",
                component: "nav_item"
              }
            ]
          }
        ],
        component: "global"
      },
      slug: "config-global",
      full_slug: "config-global",
      sort_by_date: null,
      position: 20,
      tag_list: ["config"],
      is_startpage: false,
      parent_id: 0,
      meta_data: null,
      group_id: "23c57ac5-db9c-40e8-9448-98bd920619c5",
      first_published_at: "2018-07-02T15:54:43.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Contribution",
      created_at: "2018-12-30T15:37:33.717Z",
      published_at: "2019-01-03T14:38:43.342Z",
      alternates: [
        {
          id: 478053,
          name: "Translation",
          slug: "translation",
          full_slug: "service/translation",
          is_folder: false,
          parent_id: 478048
        }
      ],
      id: 478047,
      uuid: "f3cb403d-3877-45e9-b199-2525f63c3d39",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            body: [],
            icon: "",
            logo: "",
            text: "contribution text",
            image: "",
            width: {
              _uid: "13a8c6f6-cfb3-407a-92bb-b69594a3de22",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "Contribution",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          }
        ],
        component: "page"
      },
      slug: "contribution",
      full_slug: "corporate/contribution",
      sort_by_date: null,
      position: 30,
      tag_list: [],
      is_startpage: false,
      parent_id: 478043,
      meta_data: null,
      group_id: "c1d6e204-0c9d-4969-914f-5f1cb2012d64",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Corporate Index",
      created_at: "2018-12-30T07:30:07.130Z",
      published_at: "2019-01-03T15:11:43.367Z",
      alternates: [],
      id: 477976,
      uuid: "493af7ba-9524-4a8c-80db-20b6426dab94",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            body: [
              {
                _uid: "8f82f202-2ca8-4072-a244-a91b5ef55f9b",
                body: [
                  {
                    _uid: "0dbd0daa-e99e-43e6-86b5-5ff8546ce78e",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "Recommending highly skilled and experienced personel",
                    headline: "Our experiences",
                    component: "feature",
                    background: ""
                  },
                  {
                    _uid: "f1d3507e-bec0-4616-bc75-1ad71a9a9d0e",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "A school developing interpreters and translators of the highest caliber",
                    headline: "Our staffs",
                    component: "feature",
                    background: ""
                  }
                ],
                component: "feature_list"
              }
            ],
            icon: "",
            logo: "",
            text: "This demo",
            image: "",
            width: {
              _uid: "13a8c6f6-cfb3-407a-92bb-b69594a3de22",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "Overview about us",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          }
        ],
        component: "page"
      },
      slug: "corporate",
      full_slug: "corporate/",
      sort_by_date: null,
      position: 10,
      tag_list: [],
      is_startpage: true,
      parent_id: 478043,
      meta_data: null,
      group_id: "e7b0d43e-0467-4e54-9262-1c93be051e2a",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "History",
      created_at: "2018-12-30T15:37:09.153Z",
      published_at: "2018-12-30T15:38:05.750Z",
      alternates: [
        {
          id: 478052,
          name: "Interpreting",
          slug: "interpreting",
          full_slug: "service/interpreting",
          is_folder: false,
          parent_id: 478048
        }
      ],
      id: 478046,
      uuid: "0643599f-d444-43b7-a309-a5cd761f6745",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            icon: "",
            logo: "",
            text: "This demo",
            image: "",
            width: {
              _uid: "13a8c6f6-cfb3-407a-92bb-b69594a3de22",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "History",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [
              {
                _uid: "01236506-e3c4-4da8-874d-d51afee09ae3",
                body: [
                  {
                    _uid: "6258b52d-bf3a-4332-8fb3-7ea3fa041cef",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "Recommending highly skilled and experienced personel",
                    headline: "Achievements",
                    component: "feature"
                  },
                  {
                    _uid: "25501acf-9d2a-41fd-bc92-aaea6a0d906d",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "A school developing interpreters and translators of the highest caliber",
                    headline: "History",
                    component: "feature"
                  }
                ],
                component: "feature_list"
              }
            ],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          }
        ],
        component: "page"
      },
      slug: "history",
      full_slug: "corporate/history",
      sort_by_date: null,
      position: 50,
      tag_list: [],
      is_startpage: false,
      parent_id: 478043,
      meta_data: null,
      group_id: "f6e46455-7834-41cc-a016-e07f5ccb6456",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Home",
      created_at: "2018-12-27T14:48:03.800Z",
      published_at: "2019-01-15T11:15:57.792Z",
      alternates: [],
      id: 477350,
      uuid: "7367d065-7499-411d-84c8-7141670e16c3",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "a71c6069-bbcb-40a9-bbc4-55e953496140",
            text: "Home hero text",
            buttons: [
              {
                _uid: "8bd16419-6532-4bbe-abe5-17ada5ffc1cf",
                link: {
                  id: "",
                  url: "#",
                  _uid: "2c877624-3f2e-49e5-9f91-a7df3f85366e",
                  linktype: "url",
                  fieldtype: "multilink",
                  cached_url: "#"
                },
                text: "home hero button text",
                component: "button",
                is_inline: false
              }
            ],
            headline: "JP home hero headline",
            component: "Hero",
            background: "//a.storyblok.com/f/51818/1880x1253/548b26d52e/pexels-photo-1054289.jpg",
            subheadline: "home hero sub headline"
          },
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            body: [
              {
                _uid: "79a6ef3d-8b7c-48ba-a887-e8a6db587908",
                body: [
                  {
                    _uid: "61c5ef04-7353-4df2-bed3-905d43b3109b",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    link: {
                      id: "",
                      url: "",
                      linktype: "story",
                      fieldtype: "multilink",
                      cached_url: ""
                    },
                    text: "Recommending highly skilled and experienced personel for the job",
                    headline: "Achievements",
                    component: "feature",
                    background: "//a.storyblok.com/f/51818/960x313/5038f18e0a/news.jpg"
                  },
                  {
                    _uid: "5ab85e4b-5cc6-4a31-b559-99c962810eb1",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    link: {
                      id: "0643599f-d444-43b7-a309-a5cd761f6745",
                      url: "",
                      linktype: "story",
                      fieldtype: "multilink",
                      cached_url: "corporate/history"
                    },
                    text: "A school developing interpreters and translators of highest caliber",
                    headline: "History",
                    component: "feature",
                    background: ""
                  }
                ],
                component: "feature_list"
              }
            ],
            text: "home sub hero Text",
            buttons: [],
            headline: "home sub hero headline",
            component: "call_to_action",
            background: "//a.storyblok.com/f/51818/1880x1253/548b26d52e/pexels-photo-1054289.jpg",
            bottom_body: [],
            subheadline: "home sub hero subheadline"
          },
          {
            _uid: "a9a5506c-ea27-48f6-8e5c-018e3d34ce02",
            body: [
              {
                _uid: "29787153-d9f5-4661-9598-1b504ea2da08",
                body: [
                  {
                    _uid: "39b8f993-8d2d-4ac0-aadc-6b6f2c970a0e",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    link: {
                      id: "",
                      url: "",
                      linktype: "story",
                      fieldtype: "multilink",
                      cached_url: ""
                    },
                    text: "Dependable support from Japan's top provider.",
                    headline: "Interpreting",
                    component: "feature",
                    background: ""
                  },
                  {
                    _uid: "7aff6959-7259-4f38-87c6-e1c0bc68e6ad",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    link: {
                      id: "",
                      url: "",
                      linktype: "story",
                      fieldtype: "multilink",
                      cached_url: ""
                    },
                    text: "Meeting diverse needs in a wide range of fields",
                    headline: "Translation",
                    component: "feature",
                    background: ""
                  }
                ],
                component: "feature_list"
              }
            ],
            icon: "",
            logo: "",
            text: "home services text",
            image: "",
            width: {
              _uid: "7046a2e0-9742-4b39-907b-9fafe92fdb3e",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "Services",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          },
          {
            _uid: "a9493a53-b2a1-4edb-8639-394753edaf8c",
            body: [
              {
                _uid: "0de68414-f310-47c6-b602-7bf72375557e",
                body: [
                  {
                    _uid: "6f1ce527-9d1d-4852-9a28-5385e8719bdc",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "Flexibility adaptable to all kinds of interpreting environments",
                    headline: "Interpreting Equipment",
                    component: "feature"
                  },
                  {
                    _uid: "b68b17d8-a6e0-41ae-8ca5-7ba2d574999f",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "Recommending highly skilled and experienced personel",
                    headline: "Staffing and referral",
                    component: "feature"
                  },
                  {
                    _uid: "4832da5f-b040-4e6a-ba70-b569455887ff",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "A school developing interpreters and translators of the highest caliber",
                    headline: "Academy",
                    component: "feature",
                    background: ""
                  }
                ],
                component: "feature_list"
              }
            ],
            icon: "",
            logo: "",
            text: "",
            image: "",
            width: {
              _uid: "43b1e17c-a551-4259-8d18-9340df913dc0",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          },
          {
            _uid: "6ecb5501-8a2b-4fe6-aa73-f8a2d9affdcf",
            body: [],
            text: "home / About Us / text about company",
            buttons: [
              {
                _uid: "8c0fd47a-f663-429a-9a37-429e45432f84",
                link: {
                  id: "493af7ba-9524-4a8c-80db-20b6426dab94",
                  url: "#",
                  _uid: "c025b908-e9a6-4b5a-941d-8ba51365c4cb",
                  linktype: "story",
                  fieldtype: "multilink",
                  cached_url: "corporate/"
                },
                text: "About Us",
                component: "button",
                is_inline: true
              }
            ],
            headline: "has 11 years in the business",
            component: "call_to_action",
            background: "//a.storyblok.com/f/51818/960x313/5038f18e0a/news.jpg",
            bottom_body: [],
            subheadline: ""
          }
        ],
        component: "page"
      },
      slug: "home",
      full_slug: "home",
      sort_by_date: null,
      position: 10,
      tag_list: [],
      is_startpage: false,
      parent_id: 0,
      meta_data: null,
      group_id: "bd7948d8-0bd7-4f13-8920-fb09f9b44868",
      first_published_at: "2018-06-30T09:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Interpreting",
      created_at: "2018-12-30T15:41:43.728Z",
      published_at: "2019-01-03T14:43:02.303Z",
      alternates: [
        {
          id: 478046,
          name: "History",
          slug: "history",
          full_slug: "corporate/history",
          is_folder: false,
          parent_id: 478043
        }
      ],
      id: 478052,
      uuid: "b834f122-5045-4ea0-83ab-8390138d003f",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            body: [
              {
                _uid: "2730de2b-b8ea-4bfc-a0b1-3fe203c6c6df",
                body: [
                  {
                    _uid: "fe3ed31b-8fa5-4bd4-8755-b0eb4cc05c98",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "Recommending highly skilled and experienced personel",
                    headline: "The beginning",
                    component: "feature",
                    background: ""
                  },
                  {
                    _uid: "d56e8b14-711e-48e3-aae6-f7bfed7d6c65",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "A school developing interpreters and translators of the highest caliber",
                    headline: "Recent years",
                    component: "feature",
                    background: ""
                  }
                ],
                component: "feature_list"
              }
            ],
            icon: "",
            logo: "",
            text: "This demo",
            image: "",
            width: {
              _uid: "13a8c6f6-cfb3-407a-92bb-b69594a3de22",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "History",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          }
        ],
        component: "page"
      },
      slug: "interpreting",
      full_slug: "service/interpreting",
      sort_by_date: null,
      position: -20,
      tag_list: [],
      is_startpage: false,
      parent_id: 478048,
      meta_data: null,
      group_id: "f6e46455-7834-41cc-a016-e07f5ccb6456",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Interpreting equipment",
      created_at: "2018-12-30T15:41:43.728Z",
      published_at: "2019-01-03T14:43:42.168Z",
      alternates: [
        {
          id: 478045,
          name: "Philosophy",
          slug: "philosophy",
          full_slug: "corporate/philosophy",
          is_folder: false,
          parent_id: 478043
        }
      ],
      id: 478051,
      uuid: "73b9d1a4-9479-4ad5-90d6-b70118e440b0",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            body: [
              {
                _uid: "3824f0a2-8c78-4970-91e8-18b56db8a631",
                body: [
                  {
                    _uid: "9fc35950-de1a-4618-a242-7ef791fcb71a",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "Recommending highly skilled and experienced personel",
                    headline: "Professional",
                    component: "feature",
                    background: ""
                  },
                  {
                    _uid: "af0703c8-439f-4e54-97c1-6f8e7fa998db",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "A school developing interpreters and translators of the highest caliber",
                    headline: "Carefulness",
                    component: "feature",
                    background: ""
                  }
                ],
                component: "feature_list"
              }
            ],
            icon: "",
            logo: "",
            text: "This demo",
            image: "",
            width: {
              _uid: "13a8c6f6-cfb3-407a-92bb-b69594a3de22",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "Philosophy",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          }
        ],
        component: "page"
      },
      slug: "interpreting-equipment",
      full_slug: "service/interpreting-equipment",
      sort_by_date: null,
      position: -10,
      tag_list: [],
      is_startpage: false,
      parent_id: 478048,
      meta_data: null,
      group_id: "15ad9a76-fc44-4c5e-9f55-27c483e5cd43",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Legal",
      created_at: "2018-12-30T15:28:47.194Z",
      published_at: "2019-01-03T15:34:06.126Z",
      alternates: [],
      id: 478041,
      uuid: "803ee6ad-487e-4f1e-9a6e-eb9f20b6df34",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            body: [],
            text: "Legal text",
            buttons: [],
            headline: "Legal",
            component: "call_to_action",
            background: "",
            subheadline: ""
          }
        ],
        component: "page"
      },
      slug: "legal",
      full_slug: "corporate/legal",
      sort_by_date: null,
      position: 80,
      tag_list: [],
      is_startpage: false,
      parent_id: 478043,
      meta_data: null,
      group_id: "0543310d-5677-4777-bf60-236b6297950b",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Message",
      created_at: "2018-12-30T15:36:15.410Z",
      published_at: "2018-12-30T15:39:05.124Z",
      alternates: [
        {
          id: 478050,
          name: "Staffing",
          slug: "staffing",
          full_slug: "service/staffing",
          is_folder: false,
          parent_id: 478048
        }
      ],
      id: 478044,
      uuid: "b1ec7583-2279-4f3c-9ee9-be945e748709",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            icon: "",
            logo: "",
            text: "This demo",
            image: "",
            width: {
              _uid: "13a8c6f6-cfb3-407a-92bb-b69594a3de22",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "Message from management",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [
              {
                _uid: "01236506-e3c4-4da8-874d-d51afee09ae3",
                body: [
                  {
                    _uid: "6258b52d-bf3a-4332-8fb3-7ea3fa041cef",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "Recommending highly skilled and experienced personel",
                    headline: "Achievements",
                    component: "feature"
                  },
                  {
                    _uid: "25501acf-9d2a-41fd-bc92-aaea6a0d906d",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "A school developing interpreters and translators of the highest caliber",
                    headline: "History",
                    component: "feature"
                  }
                ],
                component: "feature_list"
              }
            ],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          }
        ],
        component: "page"
      },
      slug: "message",
      full_slug: "corporate/message",
      sort_by_date: null,
      position: 70,
      tag_list: [],
      is_startpage: false,
      parent_id: 478043,
      meta_data: null,
      group_id: "d0f691e9-6c12-48de-9586-d6fd309375be",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Philosophy",
      created_at: "2018-12-30T15:36:34.080Z",
      published_at: "2018-12-30T15:38:27.001Z",
      alternates: [
        {
          id: 478051,
          name: "Interpreting equipment",
          slug: "interpreting-equipment",
          full_slug: "service/interpreting-equipment",
          is_folder: false,
          parent_id: 478048
        }
      ],
      id: 478045,
      uuid: "88e52c3d-35fd-4704-b69a-740212153f79",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            icon: "",
            logo: "",
            text: "This demo",
            image: "",
            width: {
              _uid: "13a8c6f6-cfb3-407a-92bb-b69594a3de22",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "Philosophy",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [
              {
                _uid: "01236506-e3c4-4da8-874d-d51afee09ae3",
                body: [
                  {
                    _uid: "6258b52d-bf3a-4332-8fb3-7ea3fa041cef",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "Recommending highly skilled and experienced personel",
                    headline: "Achievements",
                    component: "feature"
                  },
                  {
                    _uid: "25501acf-9d2a-41fd-bc92-aaea6a0d906d",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "A school developing interpreters and translators of the highest caliber",
                    headline: "History",
                    component: "feature"
                  }
                ],
                component: "feature_list"
              }
            ],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          }
        ],
        component: "page"
      },
      slug: "philosophy",
      full_slug: "corporate/philosophy",
      sort_by_date: null,
      position: 60,
      tag_list: [],
      is_startpage: false,
      parent_id: 478043,
      meta_data: null,
      group_id: "15ad9a76-fc44-4c5e-9f55-27c483e5cd43",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Privacy Policy",
      created_at: "2018-12-30T15:28:11.462Z",
      published_at: "2019-01-03T15:35:44.012Z",
      alternates: [],
      id: 478040,
      uuid: "46a69221-5b2a-45ac-80a6-b74f2d3bd3ff",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "d8406a04-dd0f-4897-802f-fa0d5100a372",
            body: [],
            text: "Privacy text",
            buttons: [],
            headline: "Privacy Policy",
            component: "call_to_action",
            background: "",
            subheadline: ""
          }
        ],
        component: "page"
      },
      slug: "privacy-policy",
      full_slug: "corporate/privacy-policy",
      sort_by_date: null,
      position: 20,
      tag_list: [],
      is_startpage: false,
      parent_id: 478043,
      meta_data: null,
      group_id: "84fca556-b1c8-4ff5-a4d9-aca4e87f1647",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Service",
      created_at: "2018-12-30T15:41:43.728Z",
      published_at: "2019-01-03T15:09:56.912Z",
      alternates: [],
      id: 478049,
      uuid: "9681538d-0da3-47ec-88b7-7e9e742e6183",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            body: [
              {
                _uid: "df5a0510-3d3f-4662-938e-861198cf5908",
                body: [
                  {
                    _uid: "cd90a4c4-70cb-440e-a368-d18c65d5181c",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "Recommending highly skilled and experienced personel",
                    headline: "Translation",
                    component: "feature",
                    background: ""
                  },
                  {
                    _uid: "815fd56d-3ae4-4e27-a4e5-3c956e35aacb",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "A school developing interpreters and translators of the highest caliber",
                    headline: "On-site Interpretation",
                    component: "feature",
                    background: ""
                  },
                  {
                    _uid: "6d77189d-c8ad-4edf-97b4-8a175f9eee13",
                    icon: "",
                    text: "",
                    headline: "Equipment",
                    component: "feature",
                    background: ""
                  }
                ],
                component: "feature_list"
              },
              {
                _uid: "8594ab32-5faa-4a28-b61f-329f568396d2",
                icon: "",
                text: "",
                headline: "Staffing and referral",
                component: "feature",
                background: ""
              },
              {
                _uid: "d0e3f6db-900d-4db0-8aa9-1cd8277aeb03",
                icon: "",
                text: "",
                headline: "Academy",
                component: "feature",
                background: ""
              }
            ],
            icon: "",
            logo: "",
            text: "Service text",
            image: "",
            width: {
              _uid: "13a8c6f6-cfb3-407a-92bb-b69594a3de22",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "Services we provide",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          }
        ],
        component: "page"
      },
      slug: "service",
      full_slug: "service/",
      sort_by_date: null,
      position: 10,
      tag_list: [],
      is_startpage: true,
      parent_id: 478048,
      meta_data: null,
      group_id: "2e2672ba-2946-4f44-938c-f17d1dc93113",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Staffing",
      created_at: "2018-12-30T15:41:43.728Z",
      published_at: "2019-01-03T14:44:28.690Z",
      alternates: [
        {
          id: 478044,
          name: "Message",
          slug: "message",
          full_slug: "corporate/message",
          is_folder: false,
          parent_id: 478043
        }
      ],
      id: 478050,
      uuid: "b4127745-5216-4222-a41a-565ccdb9bcd2",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            body: [
              {
                _uid: "adbe22e0-ce07-4bd6-b63d-69c4f52f47ed",
                body: [
                  {
                    _uid: "ab0195c1-01e7-4147-9354-371344c9064d",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "Recommending highly skilled and experienced personel",
                    headline: "Chairman",
                    component: "feature",
                    background: ""
                  },
                  {
                    _uid: "05cb6006-84b4-4ced-b6f8-f3489531fb76",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "A school developing interpreters and translators of the highest caliber",
                    headline: "Executives Officer",
                    component: "feature",
                    background: ""
                  }
                ],
                component: "feature_list"
              }
            ],
            icon: "",
            logo: "",
            text: "This demo",
            image: "",
            width: {
              _uid: "13a8c6f6-cfb3-407a-92bb-b69594a3de22",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "Message from management",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          }
        ],
        component: "page"
      },
      slug: "staffing",
      full_slug: "service/staffing",
      sort_by_date: null,
      position: 0,
      tag_list: [],
      is_startpage: false,
      parent_id: 478048,
      meta_data: null,
      group_id: "d0f691e9-6c12-48de-9586-d6fd309375be",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Strength",
      created_at: "2018-12-30T14:56:03.301Z",
      published_at: "2019-01-03T14:41:02.104Z",
      alternates: [],
      id: 478039,
      uuid: "8e64a7bf-2a61-477d-ab67-bce4e46ad1a6",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "efd24357-8c32-445d-b30c-0f86a8afc5fb",
            body: [],
            icon: "",
            logo: "",
            text: "",
            image: "",
            width: {
              _uid: "f22c2d29-40d5-418f-9f99-d34daf18c5ca",
              large: "",
              small: "10",
              medium: "8",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [
              {
                _uid: "4e272c60-a59e-4c1d-b7bc-115b920588e6",
                link: {
                  id: "",
                  url: "#",
                  _uid: "cd7950d2-2594-465b-adba-87e8c2f8de97",
                  linktype: "url",
                  fieldtype: "multilink",
                  cached_url: "#"
                },
                text: "Call to action",
                style: [],
                component: "button",
                is_inline: false,
                paragraph_style: ["mt-5"]
              }
            ],
            headline: "Strength 1",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "//a.storyblok.com/f/43698/3000x2000/7696f16f6b/bg_c_1.svg",
            headline_2: "In Vietnam and Japan territory ",
            bottom_body: [],
            subheadline: "Trusted by 968 companies, both sides",
            column_style: ["text-center"],
            social_icons: [],
            icon_alt_text: "",
            section_style: ["bg-dark", "fdb-viewport"],
            image_position: "",
            container_style: ["align-items-center", "justify-content-center", "d-flex"]
          },
          {
            _uid: "6f115a8c-e82d-45b9-b4b4-c209a8aa8b06",
            body: [],
            icon: "",
            logo: "",
            text: "",
            image: "",
            width: {
              _uid: "53661b78-bbc0-40d0-8ae0-bd3cf37b7222",
              large: "",
              small: "10",
              medium: "8",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [
              {
                _uid: "e6d0c716-2dff-4400-b989-271d2704f91f",
                link: {
                  id: "",
                  url: "#",
                  _uid: "219f116d-9f74-41d5-9636-8d18c290521b",
                  linktype: "url",
                  fieldtype: "multilink",
                  cached_url: "#"
                },
                text: "Call to action",
                style: [],
                component: "button",
                is_inline: false,
                paragraph_style: ["mt-5"]
              }
            ],
            headline: "Strength 2",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "In Vietnam and Japan territory ",
            bottom_body: [],
            subheadline: "Trusted by 968 companies, both sides",
            column_style: ["text-center"],
            social_icons: [],
            icon_alt_text: "",
            section_style: ["bg-dark", "fdb-viewport"],
            image_position: "",
            container_style: ["align-items-center", "justify-content-center", "d-flex"]
          },
          {
            _uid: "69d200cd-33bf-4a33-8958-7c52c8e9d036",
            body: [],
            icon: "",
            logo: "",
            text: "",
            image: "",
            width: {
              _uid: "4538cb54-b5ba-43d4-9909-90c967118d9a",
              large: "",
              small: "10",
              medium: "8",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [
              {
                _uid: "e537f180-3622-4fba-a99b-ac6ea3d67c31",
                link: {
                  id: "",
                  url: "#",
                  _uid: "ef53b4a9-fde6-41a6-ba43-962204a2b660",
                  linktype: "url",
                  fieldtype: "multilink",
                  cached_url: "#"
                },
                text: "Call to action",
                style: [],
                component: "button",
                is_inline: false,
                paragraph_style: ["mt-5"]
              }
            ],
            headline: "Strength 3 Facilitating, ",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "//a.storyblok.com/f/43698/3000x2000/7696f16f6b/bg_c_1.svg",
            headline_2: "In Vietnam and Japan territory ",
            bottom_body: [],
            subheadline: "Trusted by 968 companies, both sides",
            column_style: ["text-center"],
            social_icons: [],
            icon_alt_text: "",
            section_style: ["bg-dark", "fdb-viewport"],
            image_position: "",
            container_style: ["align-items-center", "justify-content-center", "d-flex"]
          }
        ],
        component: "page"
      },
      slug: "strength",
      full_slug: "corporate/strength",
      sort_by_date: null,
      position: 40,
      tag_list: [],
      is_startpage: false,
      parent_id: 478043,
      meta_data: null,
      group_id: "b290bd62-0e92-4f93-ac0b-50d1d047a63e",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    },
    {
      name: "Translation",
      created_at: "2018-12-30T15:41:43.728Z",
      published_at: "2019-01-03T14:42:22.939Z",
      alternates: [
        {
          id: 478047,
          name: "Contribution",
          slug: "contribution",
          full_slug: "corporate/contribution",
          is_folder: false,
          parent_id: 478043
        }
      ],
      id: 478053,
      uuid: "ade4f867-4094-4a8a-891a-9ddfb8ca768c",
      content: {
        _uid: "a6ec8a64-c6d4-409c-947a-1f44f4cb388c",
        body: [
          {
            _uid: "0d4750ce-af70-42dc-8461-431081228974",
            body: [
              {
                _uid: "ba26c7fe-cf2f-4d6a-91dc-703d40aeb9d0",
                body: [
                  {
                    _uid: "0f69074e-4e11-4a81-b3b2-5ba233b0a874",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "Recommending highly skilled and experienced personel",
                    headline: "Donating",
                    component: "feature",
                    background: ""
                  },
                  {
                    _uid: "16b66bdb-4fe2-4409-adbe-82a447a1c3c0",
                    icon: "//a.storyblok.com/f/43698/3000x3000/c744798edc/img_round.svg",
                    text: "A school developing interpreters and translators of the highest caliber",
                    headline: "Sponsor",
                    component: "feature",
                    background: ""
                  }
                ],
                component: "feature_list"
              }
            ],
            icon: "",
            logo: "",
            text: "This demo",
            image: "",
            width: {
              _uid: "13a8c6f6-cfb3-407a-92bb-b69594a3de22",
              large: "",
              small: "6",
              medium: "",
              plugin: "example_plugin",
              xlarge: "",
              xsmall: "12"
            },
            images: "",
            in_box: false,
            styles: "",
            buttons: [],
            headline: "Contribution",
            box_style: [],
            component: "call_to_action",
            row_style: ["justify-content-center"],
            background: "",
            headline_2: "",
            bottom_body: [],
            subheadline: "",
            column_style: ["text-center", "pb-md-5"],
            social_icons: [],
            icon_alt_text: "",
            section_style: [],
            image_position: "",
            container_style: []
          }
        ],
        component: "page"
      },
      slug: "translation",
      full_slug: "service/translation",
      sort_by_date: null,
      position: -30,
      tag_list: [],
      is_startpage: false,
      parent_id: 478048,
      meta_data: null,
      group_id: "c1d6e204-0c9d-4969-914f-5f1cb2012d64",
      first_published_at: "2018-07-04T11:08:37.000Z",
      release_id: null,
      lang: "default"
    }
  ]
};
