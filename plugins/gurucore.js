// CODE TIP: modify code here need to stop the nuxt process and npm run dev again

import Vue from "vue";
import packageJson from "@/package.json";
import _WhiteLabel from "@/_WhiteLabel";


// 1. add global method or property
Vue.$packageJson = packageJson;
Vue.$WhiteLabel = _WhiteLabel;


// 4. add an instance method
Vue.prototype.$packageJson = packageJson;
Vue.prototype.$WhiteLabel = _WhiteLabel;

const Gurucore = {
  // It takes the global Vue object as well as user-defined options.
  install(Vue, options) {
    // We call Vue.mixin() here to inject functionality into all components.
    // Vue.mixin({
    // Anything added to a mixin will be injected into all components.
    // In this case, the mounted() method runs when the component is added to the DOM.
    // mounted() {
    //   console.log('Mounted!')
    // }
    // })
  } // install()
};

Vue.use(Gurucore);
