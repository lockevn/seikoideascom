import Vue from 'vue'

import BlokArticle from '~/components/BlokArticle.vue'
import BlokHero from '~/components/BlokHero.vue'
import BlokParagraph from '~/components/BlokParagraph.vue'
import BlokCallToAction from '~/components/BlokCallToAction.vue'
import BlokButton from '~/components/BlokButton.vue'
import BlokFeatureList from '~/components/BlokFeatureList.vue'
import BlokFeature from '~/components/BlokFeature.vue'
import BlokHeader from '~/components/BlokHeader.vue'
import BlokFooter from '~/components/BlokFooter.vue'
import BlokNavItem from '~/components/BlokNavItem.vue'
import BlokPage from '~/components/BlokPage.vue'
import BlokProduct from '~/components/BlokProduct.vue'

import Categories from '~/components/Categories.vue'
import Markdown from '~/components/Markdown.vue'
import ColorList from '~/components/ColorList.vue'
import ArticleListItem from '~/components/ArticleListItem.vue'

import SitemapItem from '~/components/SitemapItem.vue'
import PageTitle from '~/components/PageTitle.vue'
import LanguageSelector from '~/components/LanguageSelector.vue'

import ProductList from '~/components/shop/ProductList.vue'
import ProductRelatedList from '~/components/shop/ProductRelatedList.vue'
import ProductImagesGallery from '~/components/shop/ProductImagesGallery.vue'
import ProductListItem from '~/components/shop/ProductListItem.vue'
import ProductListItemCard from '~/components/shop/ProductListItemCard.vue'
import ProductPriceDisplay from '~/components/shop/ProductPriceDisplay.vue'
import ProductBadgeTags from '~/components/shop/ProductBadgeTags.vue'
import ProductBadgeDiscount from '~/components/shop/ProductBadgeDiscount.vue'

import CategoryList from '~/components/shop/liebeco/CategoryList.vue'
import ProductSizeGuide from '~/components/shop/liebeco/ProductSizeGuide.vue'
import ProductFaceShapes from '~/components/shop/liebeco/ProductFaceShapes.vue'


Vue.component('BlokArticle', BlokArticle)
Vue.component('BlokHero', BlokHero)
Vue.component('BlokParagraph', BlokParagraph)
Vue.component('BlokCallToAction', BlokCallToAction)
Vue.component('BlokButton', BlokButton)
Vue.component('BlokFeatureList', BlokFeatureList)
Vue.component('BlokFeature', BlokFeature)
Vue.component('BlokHeader', BlokHeader)
Vue.component('BlokFooter', BlokFooter)
Vue.component('BlokNavItem', BlokNavItem)
Vue.component('BlokPage', BlokPage)
Vue.component('BlokProduct', BlokProduct)

Vue.component('Categories', Categories)
Vue.component('Markdown', Markdown)
Vue.component('ColorList', ColorList)
Vue.component('ArticleListItem', ArticleListItem)
Vue.component('SitemapItem', SitemapItem)
Vue.component('PageTitle', PageTitle)
Vue.component('LanguageSelector', LanguageSelector)


Vue.component('ProductList', ProductList)
Vue.component('ProductRelatedList', ProductRelatedList)
Vue.component('ProductImagesGallery', ProductImagesGallery)
Vue.component('ProductListItem', ProductListItem)
Vue.component('ProductListItemCard', ProductListItemCard)
Vue.component('ProductPriceDisplay', ProductPriceDisplay)
Vue.component('ProductBadgeTags', ProductBadgeTags)
Vue.component('ProductBadgeDiscount', ProductBadgeDiscount)

Vue.component('CategoryList', CategoryList)
Vue.component('ProductSizeGuide', ProductSizeGuide)
Vue.component('ProductFaceShapes', ProductFaceShapes)

