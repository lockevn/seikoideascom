import Vuex from "vuex";

const createStore = () => {
  return new Vuex.Store({
    state: {
      cacheVersion: "",      
      language: "", // store the current selected language
      languageDefault: "en",  // does not work?
      settings: {
        header: [],
        footer: []
      }
    },
    mutations: {
      setSettings(state, settings) {
        state.settings = settings;
      },

      setLanguage(state, language) {
        state.language = language;
      },
      setCacheVersion(state, version) {
        state.cacheVersion = version;
      }
    },
    actions: {
      /**
       * load the global config (layout)
       * @param {*} param0
       * @param {*} context
       */
      async loadSettings({ commit }, { version, language }) {
        let res = await this.$storyapi.get(`cdn/stories/config-global`, {
          version,
          language
        });

        commit("setSettings", res.data.story.content);
      },
      async loadCacheVersion({ commit }) {
        let res = await this.$storyapi.get(`cdn/spaces/me`);

        commit("setCacheVersion", res.data.space.version);
      }
    }
  });
};

export default createStore;
