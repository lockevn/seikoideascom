# MIDDLEWARE

A middleware in Nuxt.js lets you define a function that runs before rendering the page.
The function can be asynchronous and return a Promise so it’s ideal for loading our settings from the API.

The middleware lets you define custom function to be ran before rendering a page or a group of pages (layouts).

Middlewares will be called SERVER-side once (on the first request to the Nuxt app or when page refreshes) and CLIENT-side when navigating to further routes.

https://nuxtjs.org/guide/routing#middleware
