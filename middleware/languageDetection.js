import Vue from "vue";

// call BOTH SERVER and CLIENT side
export default (async function({ app, store, route, params, query, error, redirect, isHMR, isDev, isServer }) {
  if (isHMR) {
    // ignore if called from hot module replacement
    return;
  }

  // if (!params.lang) {
  //   return redirect("/" + route.fullPath);
  // }

  // route
  // path: '/corporate/strength',
  // hash: '',
  // query: { language: 'ja' },
  // params: { pathMatch: 'corporate/strength' },
  // fullPath: '/corporate/strength?language=ja',

  let version = query._storyblok || isDev ? "draft" : "published";
  let language = "en";
  // console.log("assume lang = store.languageDefault", store.languageDefault);

  if (!store.state.cacheVersion) {
    await store.dispatch("loadCacheVersion");
  }

  if (typeof window !== "undefined") {
    if (window.localStorage.language) {
      console.log("prefer lang from local Storage");
      language = window.localStorage.language;
    }

    if (window.StoryblokToken) {
      Vue.prototype.$storyapi.accessToken = window.StoryblokToken;
    }
  }

  if (query.language) {
    console.log("// query has the highest priority");
    language = query.language;
  }

  if (isServer) {
    store.commit("setCacheVersion", app.$storyapi.cacheVersion);
  }

  if (language !== store.state.language) {
    store.commit("setLanguage", language);

    return store.dispatch("loadSettings", {
      version,
      language
    });
  }

  console.log("middleware langdetection store.state.language", store.state.language);
  console.log("detect desire language", language);

  // If route is /<defaultLocale>/... -> redirect to /...
  if (language === store.languageDefault && route.fullPath.indexOf("/" + store.languageDefault) === 0) {
    const toReplace = "^/" + store.languageDefault + (route.fullPath.indexOf("/" + store.languageDefault + "/") === 0 ? "/" : "");
    const re = new RegExp(toReplace);
    return redirect(route.fullPath.replace(re, "/"));
  }
});
