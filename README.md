# seikoideascom

> My wondrous Nuxt.js project


## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```


## Setup

1. [Register](https://app.storyblok.com/#!/signup) at Storyblok for free.
2. Create a new Space
3. Go to Space / settings / API Key, get "preview token", put to nuxt.config.js
    Restart site to get new change in `nuxt.config.js`

4. Make sure to have the "Global" Content Entry defined: https://www.storyblok.com/tp/nuxt-js-multilanguage-website-tutorial#create-a-global-settings-content-item
4. Setup your Blog Content: https://www.storyblok.com/tp/nuxt-js-multilanguage-website-tutorial#build-a-blog-section



# development

Convention:

 * `ThisRepo / components / BlokButton` is related to the `StoryBlok.com / Component / Button`
 * Component like `ThisRepo / components / Markdown` (without Blok) is pure Vue component, and we use it to render the "markdown text property"
 * `ThisRepo / pages / articles` will handle `site / articles` (which is the slug `/articles/` ). It can read the model (in the content)
 * if we don't have appropriate `page components` in `ThisRepo / pages /`, it tries to use fallback component inside `ThisRepo / components /`


# deploy
npm run generate
then copy the dist folder to Netlify https://app.netlify.com/sites/liebeco/deploys




# code màu

Brown Nâu background-color:#804000;
Yellow Vàng background-color:#ffff00;
Trắng background-color:#ffffff;
Đen background-color:#000000;
Pink Hồng background-color:#ff00ff;
Xanh lá background-color:#00ff00;
Xanh nước biển background-color:#0080ff;
Pearl Xanh ngọc background-color:#00ffff;
Dark blue Xanh đen background-color:#112c4e;
Xám background-color:#999999;
Tím background-color:#800080;
Đỏ background-color:#ff0000;
Cam background-color:#ff8040;
Cream Kem background-color:#fef1ce;
Moss Green Xanh rêu background-color:#007000;
Hoạ tiết
Khác
Bạc background-color:#cccccc;
Skin Hồng phấn background-color:#ffc0cb;

