const StoryblokClient = require("storyblok-js-client"); // https://github.com/storyblok/storyblok-js-client

import _WhiteLabel from "./_WhiteLabel";
console.log("_WhiteLabel", _WhiteLabel);
const StoryblokToken = _WhiteLabel.PreviewStoryblokToken;
console.log("Nuxt is working with StoryblokToken", StoryblokToken);

module.exports = {
  mode: "universal",
  server: {
    port: 3000, // default: 3000
    host: "0.0.0.0" // default: localhost
  },

  /*
  ** Headers of the page
  hid is to avoid duplicated tags (which is penalized by Google)
  */
  head: {
    titleTemplate: "%s - " + _WhiteLabel.brandname,
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: _WhiteLabel.description },
      { hid: "author", name: "author", content: _WhiteLabel.author }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://use.fontawesome.com/releases/v5.0.9/css/all.css"
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: "#3B8070" },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: ["~/plugins/components", "~/plugins/filters", "~/plugins/gurucore", { src: "~/plugins/vueagile", ssr: false }],
  router: {
    middleware: "languageDetection"
  },

  /*
  ** Nuxt.js modules
  */
  modules: [
    // configure storyblock client here to interact with our space, read DRAFT (PREVIEW) data from our space
    ["storyblok-nuxt", { accessToken: StoryblokToken, cacheProvider: "memory" }],
    // Doc: https://github.com/nuxt-community/axios-module#usage
    "@nuxtjs/axios",
    // Doc:https://github.com/nuxt-community/modules/tree/master/packages/bulma
    "@nuxtjs/bulma"
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },

    optimization: {
      splitChunks: {
        chunks: "async"
      }
    },
    splitChunks: {
      pages: false,
      vendor: true,
      commons: true,
      runtime: true,
      layouts: false
    },
    /*
    ** Run ESLint on save
    */
    extend(config, ctx) {
      if (ctx.dev && process.client) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    }
  },

  // generate: {
  //   routes: [
  //     {route: '/'}
  //   ]
  // },

  generate: {
    async routes() {
      let routes = ["/"];

      const StoryblokClientInstance = new StoryblokClient({
        accessToken: StoryblokToken
      });

      let response = await StoryblokClientInstance.get("cdn/links");
      // console.log(response.data.links)
      // {
      //   '1fec2932-a2d4-4fe2-87e6-575db771b210' : {
      //     id: 478048,
      //     slug: 'service',
      //     name: 'Service',
      //     is_folder: true,
      //     parent_id: 0,
      //     published: false,
      //     position: -212945,
      //     uuid: '1fec2932-a2d4-4fe2-87e6-575db771b210',
      //     is_startpage: false }
      // }

      for (let iKey in response.data.links) {
        let linkObject = response.data.links[iKey];
        if (linkObject.slug.indexOf("config-") == 0) {
          console.log(`---------IGNORE generating for ` + linkObject.slug);
          continue;
        }

        console.log(`QUEUE ` + linkObject.slug);
        routes.push({
          route: "/" + linkObject.slug
          // payload: somethinghere will be sent as context.payload in asyncData()
        });
      }
      return routes;
    }
  } // generate
};
