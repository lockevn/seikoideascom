/**
 * contain information of specific site for each customer
 * Modify this file to have different signature for each customer
 * 
 * in component <script>, access it via this.$WhiteLabel
 * in component <template>, access it via {{ $WhiteLabel }}
 */
export default {
  author: "Lockevn",
  description: "Liébe glasses, eyewear, optical, cosmestic",
  brandname: "Liébe",
  brandwebsiteurl: "https://LiebeCo.com",
  languagesAllowed: ["en"],
  PublicStoryblokToken: "fwiBXRbILkWnwSzHSG32wAtt",
  PreviewStoryblokToken: "V0YoQPgJHzMYh796PPmHjQtt"
};
