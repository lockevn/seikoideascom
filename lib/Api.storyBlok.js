import { Util } from "@xcapital/lib-share"

export default {
  /**
   * load the viewmodel for this page, load by the path
   * put path = "/home", we will load the stories/home
   * return null if 404
   * 
   * Example: when using in asyncData()
   * 
   *  async asyncData(context) {
    
    await Api.loadStoryData({
      version: 'published',
      api: context.app.$storyapi,
      cacheVersion: context.store.state.cacheVersion,
      errorCallback: context.error,
      path: context.route.path
    })

   */
  loadStoryData: function({ api, cacheVersion, errorCallback, version, path, language }) {
    if (!path || path === "/undefined") {
      return null;
    }

    // Samplé to get JA
    // https://api.storyblok.com/v1/cdn/stories/ja/article/article-1?token=Pj17UUtgQOKBvfZLadiRvwtt&sort_by=name:asc

    let apiPath = Util.joinPath(`cdn/stories/`, language || "", path || "");
    let apiParam = {
      sort_by: "published_at:asc",
      version,
      cv: cacheVersion
    };

    // https://www.storyblok.com/docs/Delivery-Api/get-a-story
    // console.log(apiPath, apiParam);
    return api
      .get(apiPath, apiParam)
      .then(res => {
        return res.data;
      })
      .catch(res => {
        if (!res.response) {
          console.error(res);
          errorCallback({
            statusCode: 500,
            message: "Failed to receive content from api, without response"
          });
        } else {
          if (res.response.status == 404) {
            console.info(`Request to`, apiPath, apiParam, `has unexpected result`, res.response.data);
            return null;
          }

          console.error(res.response.data);
          errorCallback({
            statusCode: res.response.status,
            message: res.response.data
          });
        }
      });
  }
};
